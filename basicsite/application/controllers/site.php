<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	public function index()
	{
		//$this->load->view('view_home');
                $this->home();
	}
        
        public function home2()
	{
                $this->load->model("model_get");
                $data["result"]=  $this->model_get->getData("home");
		$this->load->view('site_header');	
                $this->load->view('site_nav');	
                $this->load->view('content_home',$data);
                $this->load->view('site_footer');
	}
        
        public function about()
	{
		$this->load->view('site_header');	
                $this->load->view('site_nav');	
                $this->load->view('content_about');
                $this->load->view('site_footer');
	}
        
        public function contact()
	{       $data["message"] = "";
                $this->load->view('site_header');	
                $this->load->view('site_nav');	
                $this->load->view('content_contact',$data);
                $this->load->view('site_footer');
	}
        
         public function send_email()
	{
		$this->load->library("form_validation");
                $this->form_validation->set_rules("fullname", "Full Name", "required|alpha");
                $this->form_validation->set_rules("email", "Email Address", "required|valid_email");
                $this->form_validation->set_rules("message", "Message", "required");
                
                if($this->form_validation ->run() == FALSE){
                    $data["message"] = "";
                    $this->load->view('site_header');	
                    $this->load->view('site_nav');	
                    $this->load->view('content_contact',$data);
                    $this->load->view('site_footer');
                }else{
                    $data["message"] = "The email has successfully been send";
                    
                    $this->load->library("email");
                    $this->email->from(set_value("email"),set_value("fullname"));
                    $this->email->to("toycollector1atom@gmail.com");
                    $this->email->subject("Message from out form");
                    $this->email->message(set_value("message"));
                    $this->email->send();
                    
                    echo $this->email->print_debugger();
                    
                    $this->load->view('site_header');	
                    $this->load->view('site_nav');	
                    $this->load->view('content_contact',$data);
                    $this->load->view('site_footer');
                    
                }
	}
}
